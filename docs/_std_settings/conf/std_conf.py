# Copyright (C) ALbert Mietus, 2015,2019 -- Copy when you like!!
#  STANDARD CONFiguration for Sphinx-doc
# -*- coding: utf-8 -*-


### Some setting depend on RTD
import os
on_rtd = os.environ.get('READTHEDOCS') == 'True'


###
### File/Project layout
###

master_doc        = 'index'
source_suffix     = '.rst'
exclude_patterns  = ['**/.#*', '**/_*']
html_static_path  = ['../_std_settings/static/',    '_static']
templates_path    = ['../_std_settings/templates/', '_templates' ]
html_css_files    = ['TechPush.css', 'local_custom.css'] # local_custom.css is for project specific custum style ; default, the file is empty


###
### Kick off
###
try:    extensions = extensions
except NameError: extensions=[]

show_authors = True

rst_epilog = None
rst_prolog = """
.. include:: /_epilog.inc

"""

if not on_rtd:
    html_sidebars = { '**': [
        'globaltoc.html',
        'searchbox.html',
        'relations.html',
        'sourcelink.html',
    ]}

if not on_rtd:
    html_theme = 'classic' # XXX This will change  --GAM


# sphinx.ext.todo
#-----------------
extensions.append('sphinx.ext.todo')
todo_include_todos=True


# sphinx.ext.autodoc, sphinxcontrib.napoleon
#-------------------------------------------

extensions.append('sphinx.ext.autodoc')
extensions.append('sphinxcontrib.napoleon') # For google- & NumPy-style docstrings

autodoc_default_flags     = ['members', 'show-inheritance']
autoclass_content         = "both"
autodoc_member_order      = "bysource"

napoleon_google_docstring = True
napoleon_numpy_docstring  = False
napoleon_use_ivar         = True
napoleon_use_rtype        = True


# plantUML
#---------
extensions.append('sphinxcontrib.plantuml')
plantuml = 'plantuml'


# Needs
#------
extensions.append('sphinxcontrib.needs')
needs_include_needs = True
needs_id_required = True
