.. (C) ALbert Mietus, Sogeti, 2019

*********************
List of Demonstrators
*********************

Each Demonstrator will be documented in it own directory. See below for the (alphabetically) list of DDS-Demonstrator
(in various stages).

.. note:: for developers

   Please create your own directory (within your Feature-branch/fork) with a name as set by the product-owner. Both in
   the :file:`src` and :file:`docs/doc/Demonstrators` directory. Use the doc-directory to document the Demonstrator.

   See the :ref:`TeamPages` directory for “scratch” documentation on personal/team-level. Also, create a subdir there.

.. toctree::
   :maxdepth: 2
   :glob:

   */index



