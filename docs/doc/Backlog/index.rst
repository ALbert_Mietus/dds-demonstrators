======================
Backlog & requirements
======================

Here you find a *backlog* of the DDS-Demonstrators; it will be updated/maintained by the *Product Owner*\(s).

.. tip::

   When, as part of a project you feel the need to add a Demonstrator-idea to the backlog, plz make a proposal in your :ref:`TeamPages`!

   When accepted it will be moved to here.

   .. warning:: When using `needs`, tag it with ‘``idea``’!

.. toctree::
   :maxdepth: 2
   :glob:

   *
   */index
